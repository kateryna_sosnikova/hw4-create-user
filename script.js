let name = prompt('Enter your name');
const surname = prompt('Enter your surname');

function createNewUser(name, surname) {
    let newUser = {
        firstName: name,
        lastName: surname,

        getLogin() {
            console.log(`${(this.firstName[0] + this.lastName).toLowerCase()}`);
        },

        setFirstName(newFirstName) {
            this.firstName = newFirstName;
        },
    };

    Object.defineProperty(newUser, 'firstName', {
        value: name,
        writable: false
    });

    Object.defineProperty(newUser, 'lastName', {
        value: surname,
        writable: false
    });
    
    /*
    Object.defineProperty(newUser, 'firstNameNew', {
        enumerable: true,
        configurable: false,
        writable: false,
        set(val) {
            this.firstName = val;
        },
        get() {
            return this.firstName;
        },
    });

setFirstName(newFirstName) {
this.firstName = newFirstName;
Object.defineProperty(newUser, 'firstName', {
    value: name,
    writable: false,
    });
}

setLastName(newLastName) {
        this.lastName = newLastName;
    },
}

*/

newUser.firstName = 'NEW NAME'; //doesn't change firstname value

    return newUser;
}

//console.log(createNewUser(name, surname).setFirstName(kate_new));

let kate = createNewUser(name, surname);
kate.setFirstName('CHANGED_NAME_VIA_METHOD'); 
console.log(kate.getLogin());
// DOESN'T WORK!?

